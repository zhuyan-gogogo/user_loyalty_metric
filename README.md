Package information: [![Python 2.7](https://img.shields.io/badge/python-2.7-blue.svg)](https://www.python.org/download/releases/2.7/)
[![Python 3.6](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release/python-360/)
[![License: LGPL v3](https://img.shields.io/badge/license-LGPL%20v3-blue.svg)](http://www.gnu.org/licenses/lgpl-3.0)
[![PyPI version](https://badge.fury.io/py/TPOT.svg)](https://badge.fury.io/py/TPOT)

TPOT: a Python Automated Machine Learning tool that optimizes machine learning pipelines using genetic programming.

Reference: [TPOT github page](https://github.com/rhiever/tpot)
TPOT will automate the most tedious part of machine learning by intelligently exploring thousands of possible pipelines to find the best one for your data.


## Installation of dependency

#### Install NumPy, SciPy, scikit-learn ####
from pip
#### Install Xgboost ####
[MacOS](https://www.ibm.com/developerworks/community/blogs/jfp/entry/Installing_XGBoost_on_Mac_OSX?lang=en)

* follow install procedure; check `gcc --version`

EC2:

* switch to root user `sudo su - root`
* follow the same procedure.
* install gcc `sudo yum groupinstall "Development Tools"`

[TPOT installation instructions](http://rhiever.github.io/tpot/installing/) in the documentation.
Recommened installation through: Anaconda Python distribution


## Usage
### Generate data
`generateDailyData.sql`
* Append the data in the Redshift table
* Note: might cause error when try to generate data earlier than 1 day ago
`patch2.sql`
* Generate data from 7 days ago and insert into main table
* Note: check before run the delete

OR

`generateTrainingData.sql`
* Drop the existing table with last training dataset, create a new table, unload into s3. Have to move to EC2 before evaluating.
* Main data contains meetings data 7 days ago and Target for the prediction model is data from yesterday
* Note: modify the file name accordingly


### Workflow pipeline ###
#### Weekly: update the `rpt.user_loyalty_metric` table ####
`generatePrediction.sql`

#### Monthly or Quarterly: train the model again####

### Optional ###
#### Get a new best model based on training data ####
`selectModelAtt.py` and `selectModelHost.py`
* New model will be saved.
* Note: modify the files name accordingly. Cannot use the file to evaluate directly.


### Evaluate the chosen model on future data
`evaluateAttendPipeline.py` and `evaluateHostPipeline.py`
* If a new best model is generated from Optional step, copy the ```exported_pipeline = ``` line to above scripts. Requires two training data file with columns `next_attend` and `next_host`, one is 7 days' ago's data (main data: 14 days ago, target data: 7 days ago), one is 1 day ago's data.
* If just want to test whether the current model still accurate, put training data from `generateTrainingData.sql` in both ```training_data_file = ``` and  ```evaluate_data_file```. Same
* Fitted model saved `model/`